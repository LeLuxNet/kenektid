package kenektid

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSuccess(t *testing.T) {
	for _, s := range HTTPAll {
		t.Run(s.URL, func(t *testing.T) {
			r, u, err := s.Check()
			assert.Nil(t, err)
			assert.Nil(t, u)
			assert.True(t, r)
		})
	}
}
