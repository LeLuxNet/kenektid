package kenektid

import (
	"io"
	"net/http"
	"net/url"
	"strings"
)

var (
	HTTPGoogle = HTTP{
		URL:        "http://connectivitycheck.gstatic.com/generate_204",
		StatusCode: http.StatusNoContent,
	}

	HTTPApple = HTTP{
		URL:        "http://captive.apple.com",
		StatusCode: http.StatusOK,
		Body:       "<HTML><HEAD><TITLE>Success</TITLE></HEAD><BODY>Success</BODY></HTML>\n",
	}

	HTTPMicrosoft = HTTP{
		URL:        "http://www.msftncsi.com/ncsi.txt",
		StatusCode: http.StatusOK,
		Body:       "Microsoft NCSI",
	}

	HTTPNintendo = HTTP{
		URL:        "http://conntest.nintendowifi.net",
		StatusCode: http.StatusOK,
		Body: strings.Join([]string{
			"",
			`<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">`,
			"<html>",
			"<head>",
			"<title>HTML Page</title>",
			"</head>",
			`<body bgcolor="#FFFFFF">`,
			"This is test.html page",
			"</body>",
			"</html>\n          "}, "\n            "),
	}

	HTTPFirefox = HTTP{
		URL:        "http://detectportal.firefox.com/success.txt",
		StatusCode: http.StatusOK,
		Body:       "success\n",
	}

	HTTPPopOS = HTTP{
		URL:        "http://204.pop-os.org",
		StatusCode: http.StatusNoContent,
	}

	HTTPAll = []HTTP{
		HTTPGoogle,
		HTTPApple,
		HTTPMicrosoft,
		HTTPNintendo,
		HTTPFirefox,
		HTTPPopOS,
	}
)

type HTTP struct {
	URL        string
	StatusCode int
	Body       string
}

func (s HTTP) Check() (bool, *url.URL, error) {
	res, err := http.Get(s.URL)
	if err != nil {
		return false, nil, err
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case http.StatusFound:
		u, err := res.Location()
		if err != nil {
			return false, nil, err
		}
		return false, u, nil
	case s.StatusCode:
	default:
		return false, nil, nil
	}

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return false, nil, err
	}

	return string(b) == s.Body, nil, nil
}

func (s HTTP) CheckAndSolve() (bool, error) {
	ok, u, err := s.Check()
	if err != nil {
		return false, err
	} else if ok {
		return true, nil
	} else if u == nil {
		return false, nil
	}

	return Solve(u)
}
