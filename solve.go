package kenektid

import (
	"errors"
	"net/url"
)

var (
	ErrNotSolved = errors.New("captcha could not be solved")
)

func Solve(u *url.URL) (bool, error) {
	if s, ok := solver[u.Host]; ok {
		return s(u)
	}

	return false, ErrNotSolved
}

var solver = map[string](func(u *url.URL) (bool, error)){}

func RegisterSolver(host string, fn func(u *url.URL) (bool, error)) {
	solver[host] = fn
}
