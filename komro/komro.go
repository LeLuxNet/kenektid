package komro

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"

	"lelux.net/kenektid"
)

var endpoint = "https://citywlan.komro.net/portal_api.php"

func Solve(u *url.URL) (bool, error) {
	fmt.Println(u)

	data := url.Values{}
	data.Set("action", "subscribe")
	data.Set("type", "one")

	res, err := http.Post(endpoint, "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	if err != nil {
		return false, err
	}
	defer res.Body.Close()

	b, err := io.ReadAll(res.Body)
	if err != nil {
		return false, err
	}

	fmt.Println(b)

	return false, nil
}

func init() {
	kenektid.RegisterSolver("citywlan.komro.net", Solve)
}
